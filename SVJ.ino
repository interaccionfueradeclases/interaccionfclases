int SensorPresionE = A0; 
int SensorPresionS = A1;// Sensores conectados a A0 y A1
int LEDpin1 = 6;
int LEDpin2 = 7;// LEDs entradas 6 y 7
int LecturaSPE,LecturaSPS;    
int jI,jA,jS;

void setup()
{
Serial.begin(9600); // Conexion a puerto serial 
pinMode(SensorPresionE, INPUT);
pinMode(SensorPresionS, INPUT);
pinMode(LEDpin1, OUTPUT);
pinMode(LEDpin2, OUTPUT);
}


void loop() {
    
    int mensaje = mensajePHP();
    ejecutarMensaje(mensaje);   
    RegistrosSP();
    EnvioRegistros();
}


void RegistrosSP()
{
  LecturaSPE = analogRead(SensorPresionE);
  LecturaSPS = analogRead(SensorPresionS);
  
  if(LecturaSPE>0){
       jI=jI+1;
       digitalWrite(LEDpin1 , HIGH);
       delay(100);
       digitalWrite(LEDpin1 , LOW);
       }
  if(LecturaSPS>0){
       jS=jS+1;
       digitalWrite(LEDpin2 , HIGH);
       delay(100);
       digitalWrite(LEDpin2 , LOW);
       }
  jA=jI-jS;
}

void EnvioRegistros(){
  Serial.println("Enviando registros..");
  Serial.println("JI:"+jI);
  Serial.println("JA:"+jA);
  Serial.println("JS:"+jS);
  
  
  }

int mensajePHP() {   
  int mensaje = 0;
  
  if (Serial.available() > 0) {
    mensaje = Serial.parseInt();
  }
  
  return mensaje;
}

void ejecutarMensaje(int mensaje) {
    
    switch(mensaje){
      case 0:
        break;
      case 100://reseteo
        jI=0;jA=0;jS=0;
        break;
      case 101://Solicitud de datos
        EnvioRegistros();
        break;
      default:
        break;
    }
}
